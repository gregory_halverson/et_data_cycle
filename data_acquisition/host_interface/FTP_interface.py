__author__ = 'Gregory Halverson'

import os
import ftplib
from ftplib import FTP
from urlparse import urlparse

class FTP_Interface:
    def __init__(self, url):
        parsed_uri = urlparse(url)
        self.domain = parsed_uri.netloc

        print('connecting to \'' + self.domain + '\'')

        self.ftp = FTP(self.domain)
        login_response = self.ftp.login()

        if login_response == '230 Login successful.':
            print('login successful')
        else:
            print('login failed')

    def directory_listing(self, path):
        try:
            self.ftp.cwd(path)
        except:
            return []

        try:
            return self.ftp.nlst()
        except ftplib.error_perm, resp:
            if str(resp) == "550 No Files Found":
                print('path \'' + path + '\' is empty')

            return []

    def download_file(self, path, filename, local_path):
        try:
            self.ftp.cwd(path)
        except:
            print('unable to navigate to \'' + path + '\'')

        if not os.path.exists(local_path):
            os.makedirs(local_path)

        with open(os.path.join(local_path, filename), 'wb') as local_file:
            retr_command = 'RETR ' + filename
            self.ftp.retrbinary(retr_command, local_file.write)