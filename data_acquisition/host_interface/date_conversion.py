from datetime import date, timedelta

__author__ = 'Gregory'


def date_to_yday(date):
    time_tuple = date.timetuple()
    day_of_year = time_tuple.tm_yday
    year = time_tuple.tm_year

    return year, day_of_year


def yday_to_date(year, day_of_year):
    return date(year, 1, 1) + timedelta(day_of_year - 1)