__author__ = 'Gregory Halverson'

from data_acquisition.LAADS import LAADS

laads = LAADS()

product_name = 'MOD04_L2'
local_path = 'data'

date = laads.most_recent_date(product_name)

print('most recent date available for \'' + product_name + '\' is ' + str(date))

laads.download_date(product_name, date, local_path)