__author__ = 'Gregory Halverson'

from urlparse import urlparse

from data_acquisition.host_interface.date_conversion import date_to_yday, yday_to_date
from data_acquisition.host_interface.FTP_interface import FTP_Interface

class LAADS:
    base_address = 'ftp://ladsweb.nascom.nasa.gov/allData/51/'

    def __init__(self):
        parsed_uri = urlparse(LAADS.base_address)
        self.domain = parsed_uri.netloc
        self.base_path = parsed_uri.path

        self.ftp = FTP_Interface(LAADS.base_address)

        self.products_available = self.get_products_available()

    def get_products_available(self):
        product_list = self.ftp.directory_listing(self.base_path)

        return product_list

    def is_product_available(self, product_name):
        return product_name in self.products_available

    def get_years_available(self, product_name):
        path = self.base_path + '/' + product_name + '/'
        years_available = self.ftp.directory_listing(path)
        years_available.remove('NOTICE')

        return [int(year) for year in years_available]

    def is_year_available(self, product_name, year):
        return year in self.get_years_available(product_name)

    def get_days_available(self, product_name, year):
        path = self.base_path + '/' + product_name + '/' + str(year) + '/'
        days_available = self.ftp.directory_listing(path)

        return [int(day) for day in days_available]

    def is_day_available(self, product_name, year, day):
        return day in self.get_days_available(product_name, year)

    def is_date_available(self, product_name, date):
        year, day_of_year = date_to_yday(date)

        return day_of_year in self.get_days_available(product_name, year)

    def most_recent_date(self, product_name):
        year = max(self.get_years_available(product_name))
        day_of_year = max(self.get_days_available(product_name, year))
        date = yday_to_date(year, day_of_year)

        return date

    def get_files_available(self, product_name, year, day):
        path = self.base_path + '/' + product_name + '/' + str(year) + '/' + ("%03d" % (day))
        files_available = self.ftp.directory_listing(path)

        return files_available

    def download_date(self, product_name, date, local_path):
        year, day_of_year = date_to_yday(date)
        file_list = self.get_files_available(product_name, year, day_of_year)
        path = self.base_path + '/' + product_name + '/' + str(year) + '/' + ("%03d" % (day_of_year))

        for filename in file_list:
            print('downloading \'' + filename + '\'')
            self.ftp.download_file(path, filename, local_path)
